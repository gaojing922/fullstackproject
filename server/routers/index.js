const KoaRouter = require('koa-router')
const md5 = require('md5')
// 通过Models(包含各种定义好的模型对象)操作数据
const Models = require('../models')
const Sequelize = require('sequelize')
// 创建路由
const router = new KoaRouter()
// router.get('/', async ctx => {
//   // 操作数据库 读取数据 读取id为1的并且id为1的评论数据 关联查询
//   const data = await Models.Users.findByPk(1, {
//     include: {
//       model: Models.Contents
//     }
//   })
//   console.log(data)
//   ctx.body = {
//     data,
//     code: 0
//   }
// })
// 获取评论列表
router.get('/getList', async ctx => {
  // const currPage = ctx.request.body.currPage || 1 // 当前页如果是post请求
  const currPage = Number(ctx.query.currPage) || 1 // 当前页get请求
  const pageSize = 5 // 每页显示几条
  // 查询总数
  const total = await Models.Contents.findAndCountAll()
  const totalCount = total.count // 总条数
  const totalPage = Math.ceil(totalCount / pageSize) // 总页数
  // 操作数据库 读取数据 读取id为1的并且id为1的评论数据 关联查询
  const data = await Models.Contents.findAll({
    include: {
      model: Models.Users
    },
    offset: (currPage - 1) * pageSize,
    limit: pageSize
  }).map(item => ({
    id: item.id,
    user_id: item.User.id,
    title: item.title,
    content: item.content,
    username: item.User.username,
    updatedAt: item.updatedAt,
    comment_count: item.comment_count,
    like_count: item.like_count
  }))
  ctx.body = {
    data,
    code: 0,
    pages: {
      totalCount,
      totalPage,
      pageSize,
      currPage
    }
  }
})

// 登录
router.post('/login', async ctx => {
  let {username, password} = ctx.request.body
  if (username.trim() === '' || password.trim() === '') {
    ctx.body = {
      code: 1,
      message: '参数错误'
    }
  } else {
    let user = await Models.Users.findOne({
      where: {
        username
      }
    })
    if (user === null) {
      ctx.body = {
        code: 1,
        message: '用户不存在'
      }
    } else {
      if (user.get('password') !== md5(password)) {
        ctx.body = {
          code: 1,
          message: '密码错误！'
        }
      } else {
        // 服务端发送一个cookie 表示当前是登录的
        ctx.session.user_id = user.get('id')
        ctx.session.username = user.get('username')
        ctx.body = {
          code: 0,
          data: {
            id: user.get('id'),
            username: user.get('username')
          },
          message: '登录成功'
        }
      }
    }
  }
})

// 注册
router.post('/register', async ctx => {
  let {username, password, rePassword} = ctx.request.body
  if (username.trim() === '' || password.trim() === '' || rePassword.trim() === '') {
    ctx.body = {
      code: 1,
      message: '参数错误'
    }
  } else {
    if (password.trim() !== rePassword.trim()) {
      ctx.body = {
        code: 1,
        message: '两次输入的密码不一致！'
      }
    } else {
      let user = await Models.Users.findOne({
        where: {
          username
        }
      })
      if (user) {
        ctx.body = {
          code: 1,
          message: '当前用户已存在！'
        }
      } else {
        // 将用户保存至数据库
        let newUser = await Models.Users.build({
          username,
          password: md5(password)
        }).save()
        ctx.body = {
          code: 0,
          data: {
            id: newUser.get('id'),
            username: newUser.get('username')
          },
          message: '注册成功！'
        }
      }
    }
  }
})

// 判断用户是否登录
router.get('/isLogin', async ctx => {
  let user_id = ctx.session.user_id // koa-session会自动解密
  let username = ctx.session.username
  if (typeof user_id === null || !user_id) {
    ctx.body = {
      code: 1,
      message: '用户未登录'
    }
  } else {
    ctx.body = {
      code: 0,
      message: '用户已经登录',
      data: {
        id: user_id,
        username
      }
    }
  }
})

// 退出登录
router.post('/logout', async ctx => {
  let {user_id, username} = ctx.request.body
  if (typeof user_id !== null || typeof username !== null) {
    ctx.session.user_id = null
    ctx.session.username = null
    ctx.body = {
      code: 0,
      message: '退出成功！'
    }
  } else {
    ctx.body = {
      code: 1,
      message: '退出失败！'
    }
  }
})

// 点赞
router.post('/like', async ctx => {
  let { content_id } = ctx.request.body // 要点赞的内容ID跟点赞用户id
  let user_id = ctx.session.user_id
  console.log(content_id)
  console.log(user_id)
  if (!user_id) {
    return ctx.body = {
      code: 1,
      data: '您还有没登录!'
    }
  }
  // 获取当前被点赞的内容
  let content = await Models.Contents.findByPk(content_id)
  if (!content) {
    return ctx.body = {
      code: 1,
      message: '没有对应的内容'
    }
  }
  // 查询当前用户是否对该内容进行过点赞
  let like = await Models.Likes.findOne({
    where: {
      [Sequelize.Op.and]: [
        {'content_id': content_id},
        {'user_id': user_id},
      ]
    }
  })
  if (like) {
    return ctx.body = {
      code: 1,
      message: '您已经点过赞了!'
    }
  }
  // 对内容的点赞数据进行增加
  content.set('like_count', content.get('like_count') + 1)
  await content.save()
  // 将点赞的信息更新至点赞表 Likes
  await Models.Likes.build({
    content_id,
    user_id
  }).save()
  ctx.body = {
    code: 0,
    message: '点赞成功！',
    data: content
  }

})

module.exports = router