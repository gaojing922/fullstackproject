'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('Contents', [
      {
        user_id: 1,
        title: '成年人的爱情，懂事，真的很重要',
        content: '纵使对方足够爱你，能理解你的“不懂事”，也该明白，爱情不仅需要互相偎依，也需要各有空间。 成年人的爱情，尤为如此。 之前逛某论坛时，经常会看到这',
        comment_count: 10
      },
      {
        user_id: 2,
        title: '疫情结束了，我想去离婚',
        content: '晚上闺蜜发来消息，说过了疫情，要去办离婚手续，这回彻底心凉了。 闺蜜两口子都是事业单位职工，两人有个儿子今年上高一，日子一直过得安宁舒适，至少外',
        comment_count: 1
      },
      {
        user_id: 1,
        title: '实录：再婚后我生了两个儿子，却不敢告诉丈夫我还有个女儿',
        content: '01 曾经，我有个很幸福的家庭，老公做生意，我当老师，我们有个活泼可爱的女儿。 我和老公是自由恋爱，谈婚论嫁也很顺利，虽然日子过得很平淡，但我总',
        comment_count: 3
      },
      {
        user_id: 2,
        title: '因为10个口罩，我看到了妻子的另一面，终止了两年的婚外恋',
        content: '01 作家詹迪·尼尔森曾说：“遇见灵魂伴侣的感觉，就好像是走进一座你曾经住过的房子里——你认识那些家具，认识墙上的画，架上的书，抽屉里的东西：如',
        comment_count: 2
      },
      {
        user_id: 1,
        title: '读书变现技能养成记（二）——如何把读书升级成专业吸金技能',
        content: '先要对职业读书人的背景有两个认识： 1、职业读书人得益于现代的信息爆炸，更侧重于筛选。所以职业读书人能产生拆选价值。 2、行业还处于野蛮生长期',
        comment_count: 2
      },
      {
        user_id: 1,
        title: '读书变现技能养成记（二）——如何把读书升级成专业吸金技能',
        content: '先要对职业读书人的背景有两个认识： 1、职业读书人得益于现代的信息爆炸，更侧重于筛选。所以职业读书人能产生拆选价值。 2、行业还处于野蛮生长期',
        comment_count: 2
      },
      {
        user_id: 2,
        title: '读书变现技能养成记（二）——如何把读书升级成专业吸金技能',
        content: '先要对职业读书人的背景有两个认识： 1、职业读书人得益于现代的信息爆炸，更侧重于筛选。所以职业读书人能产生拆选价值。 2、行业还处于野蛮生长期',
        comment_count: 2
      },
      {
        user_id: 1,
        title: '读书变现技能养成记（二）——如何把读书升级成专业吸金技能',
        content: '先要对职业读书人的背景有两个认识： 1、职业读书人得益于现代的信息爆炸，更侧重于筛选。所以职业读书人能产生拆选价值。 2、行业还处于野蛮生长期',
        comment_count: 2
      },
      {
        user_id: 2,
        title: '读书变现技能养成记（二）——如何把读书升级成专业吸金技能',
        content: '先要对职业读书人的背景有两个认识： 1、职业读书人得益于现代的信息爆炸，更侧重于筛选。所以职业读书人能产生拆选价值。 2、行业还处于野蛮生长期',
        comment_count: 15
      }
    ], {})
  },
  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
