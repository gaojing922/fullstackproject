(async function() {
  const koa = require('koa')
  const KoaStaticCache = require('koa-static-cache')
  const KoaBodyParser = require('koa-bodyparser')
  const app = new koa()
  const Session = require('koa-session')
  const router = require('./routers')
  // 设置session,登录鉴权
  app.use(Session({
    key: 'koa:sess',
    maxAge: 15 * 60 * 3600,
    autoCommit: true,
    overwrite: true,
    httpOnly: true,
    signed: true,
    rolling: false,
    renew: false
  }, app))
  app.keys = ['i am gaojing']
  // 处理跨域 前端可以用webpack proxy代理
  // app.use(async (ctx, next) => {
  //    ctx.set('Access-Control-Allow-Origin', '*')
  //    await next()
  // })
  // 处理静态资源
  app.use(KoaStaticCache('./static', {
    prefix: 'static',
    gzip: true
  }))
  // 处理post响应数据
  app.use(KoaBodyParser())
  // 处理路由
  app.use(router.routes())
  app.listen(2323, '0.0.0.0', () => {
    console.log('服务已启动: 0.0.0.0:2323')
  })
})()