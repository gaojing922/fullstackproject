'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Comments', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      content_id: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      content: {
        type: Sequelize.STRING(1000),
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: '2020-03-18 12:00:00'
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: '2020-03-20 12:00:00'
      }
    }, {
      tableName: 'comments',
      charset: 'utf8mb4'
      // collate: 'utfmb4_bin'
    }).then(() => {
      // 给comments表添加索引
      return queryInterface.addIndex('comments', {
        name: 'user_id', // 索引名称
        fields: ['user_id'] // 索引对应数据库表里面的字段
      })
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Comments');
  }
};