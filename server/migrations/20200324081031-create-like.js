'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Likes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      content_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: '2020-03-22 12:00:00'
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: '2020-03-24 12:00:00'
      }
    }, {
      tableName: 'likes',
      charset: 'utf8mb4'
      // collate: 'utfmb4_bin'
    }).then(() => {
      // 给likes表添加索引
      return queryInterface.addIndex('likes', {
        name: 'content_id', // 索引名称
        fields: ['content_id'] // 索引对应数据库表里面的字段
      })
    }).then(() => {
      // 给likes表添加索引
      return queryInterface.addIndex('likes', {
        name: 'user_id', // 索引名称
        fields: ['user_id'] // 索引对应数据库表里面的字段
      })
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Likes');
  }
};