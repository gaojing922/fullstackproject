'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      username: {
        type: Sequelize.STRING(20),
        allowNull: false
      },
      password: {
        allowNull: false,
        type: Sequelize.CHAR(200)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: '2019-03-24 12:00:00'
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: '2018-08-14 12:00:00'
      }
    }, {
      tableName: 'users',
      charset: 'utf8mb4'
      // collate: 'utfmb4_bin'
    }).then(() => {
      // 给users表添加索引
      return queryInterface.addIndex('users', {
        name: 'username', // 索引名称
        unique: true, // 该索引的值为唯一
        fields: ['username'] // 索引对应数据库表里面的字段
      })
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Users');
  }
}