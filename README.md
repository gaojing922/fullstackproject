# author 高景
# fullStackProject

全栈项目,登录，注册，评论，点赞
后端：
  node + koa + mysql + sequelize
前端：
  vue全家桶 + axios + element-ui

## 客户端操作
  # 项目本地运行
    -- 1 cd client 进入client目录
    -- 2 npm install
    -- 3 npm start
  # 项目打包上线
    -- 1 cd client 进入client目录
    -- 2 npm run dll
    -- 3 npm run build
  # 注意
    -- 项目在第一次打包的时候，必须要npm run dll,这个命令是预打包，可以把项目中不常用到的文件(vue, vue-router,axios...)，打包到一个指定文件里面去，
    -- 在运行npm run build打包的时候会直接从指定文件里去读内容，节省打包时间，如果上述文件有更改或者版本更新，必须再次执行npm run dll
## 服务端操作
  # 项目本地运行
    -- 0 本地一定得安装mysql数据库, 然后进入server/config 修改development(默认)下面的数据库连接配置，也就是连到你自己本地的数据库
    -- 1 cd server 进入server目录
    -- 2 npm install
    -- 3 运行数据库迁移文件, 创建数据库(如果想手动创建，可以忽略这一步，但是数据库结构一定要按照文件== 数据库结构.md == 里面的结构设计)
         运行数据库迁移文件命令: ./node_modules/.bin/sequelize db:migrate (因为sequelize-cli不是全局安装，所以要指定路径./node_modules/.bin/sequelize)
    -- 4 创建种子文件(目的是为了往第三步创建好的数据库表里面批量插入数据,如果想手动插入,可以忽略这步)
         创建种子文件命令 ./node_modules/.bin/sequelize seed:generate --name Contents
         运行种子文件命令 ./node_modules/.bin/sequelize db:seed:all 或者指定种子文件名称 ./node_modules/.bin/sequelize db:seed --seed xxxx-xxxx.js
    -- 5 npm start  启动服务