// vendor文件预打包
const webpack = require("webpack")
const config = require('./config')
module.exports = {
  entry: {
    // 把 vue 相关模块的放到一个单独的动态链接库 按需要引入的库不能通过dll打包
    vue: ["vue", "vue-router", "axios", "vuex"]
  },
  output: {
      filename: "[name]-[hash].dll.js", // 生成vue.dll.js
      path: config.dllPath,
      library: "dll_[name]"
  },
  plugins: [
    new webpack.DllPlugin({
        name: "dll_[name]",
        // manifest.json 描述动态链接库包含了哪些内容
        path: config.vueDllManifestJson
    })
  ]
}