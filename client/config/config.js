// 公用配置
const path = require('path')
module.exports = {
  entryPath: path.resolve(__dirname, '../src/main.js'), // 入口文件
  assetsRoot: path.resolve(__dirname, '../dist'), // 正式环境打包文件输出路径
  assetsImagesDirectory: 'images/', // 图片打包出来的图片目录
  assetsCssDirectory: 'css/', // css打包出来的图片目录
  assetsJsDirectory: 'js/', // js打包出来的图片目录
  publicPath: '/',
  indexPath: path.resolve(__dirname, '../src/index.ejs'), // html模版路径
  dllPath: path.resolve(__dirname, "../dll"), // dll 预打包存放目录
  dllFilepath: path.resolve(__dirname, "../dll/*.js"), // 打包的时候具体打包dll文件
  vueDllManifestJson: path.resolve(__dirname, '../dll/vue.dll.manifest.json'), // dll预打包json文件存放路径
  ivewDllManifestJson: path.resolve(__dirname, '../dll/elementUi.dll.manifest.json'), // dll预打包json文件存放路径
  commonInclude: path.resolve(__dirname, '../src/'), // 插件解析指定目录
  proxyUrl: 'http://localhost:1688/' // 代理地址路径
}