const path = require('path')
const merge = require('webpack-merge')
const webpack = require('webpack')
const baseConfig = require('./webpack.base')
const MiniCssExtractPlugin = require('mini-css-extract-plugin') // 分离css
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin') // 压缩css
const ParallelUglifyPlugin = require('webpack-parallel-uglify-plugin') // 压缩Js
const AddAssetHtmlPlugin = require('add-asset-html-webpack-plugin')
const config = require('./config')
module.exports = merge(baseConfig, {
  mode: 'production', // 开发模式
  // 入口
  output: {
    filename: 'js/[name].[contenthash:4].js',
    path: config.assetsRoot,
    chunkFilename: "js/[id].[contenthash:4].js"
  },
  devtool: false,
  module: {
    rules: [
      {
        test: /\.(c|le)ss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              // 这里可以指定一个 publicPath
              // 默认使用 webpackOptions.output中的publicPath
              // publicPath的配置，和plugins中设置的filename和chunkFilename的名字有关
              // 如果打包后，background属性中的图片显示不出来，请检查publicPath的配置是否有误
              publicPath: '../',
              hmr: false // 仅dev环境启用HMR功能
            },
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true // 一般配置在开发环境 定位css在哪个文件
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              sourceMap: true,
              plugins: (loader) => [
                require('autoprefixer') // 给css3属性添加前缀
              ]
            }
          },
          'less-loader'
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'css/[name].[hash].css' // 设置最终输出的文件名
      // chunkFilename: config.assetsCssDirectory + '[id].[hash].css'
    }),
    new OptimizeCSSAssetsPlugin({}), // 压缩css
    // vue映射
    new webpack.DllReferencePlugin({
      manifest: config.vueDllManifestJson
    }),
    new AddAssetHtmlPlugin([{ // 往html中注入dll js
      publicPath: "dll/",  // 注入到html中的路径
      outputPath: "dll", // 最终输出的目录
      filepath: config.dllFilepath,
      includeSourcemap: false,
      typeOfAsset: "js" // options js、css; default js
    }])
  ],
  optimization: {
    splitChunks: {
      chunks: "async", // 共有三个值可选：initial(初始模块)、async(按需加载模块)和all(全部模块)
      minSize: 30000, // 模块超过30k自动被抽离成公共模块
      minChunks: 1, // 模块被引用>=1次，便分割
      maxAsyncRequests: 5,  // 异步加载chunk的并发请求数量<=5
      maxInitialRequests: 3, // 一个入口并发加载的chunk数量<=3
      name: true, // 默认由模块名+hash命名，名称相同时多个模块将合并为1个，可以设置为function
      automaticNameDelimiter: '~', // 命名分隔符
      cacheGroups: {
        styles: {
          name: 'styles',
          test: /\.(c|le)ss$/,
          chunks: 'all',
          enforce: true,
        },
        vendors: {
          chunks: "all", // 使用 all 模式
          test: /[\\/]node_modules[\\/]/, // 匹配 node_modules 下的模块
          name: "vendors", // 包命名，最终的命名要结合 output 的 chunkFilename
          minChunks: 1,
          minSize: 30000,
          priority: 10 // 设置优先级
        },
        async: {
          chunks: "async",
          minChunks: 1, // 代码块至少被引用的次数
          maxInitialRequests: 3, // 设置最大的请求数
          minSize: 0, // 设置每个 chunk 最小的大小 (默认30000)，这里设置为 0，以方便测试
          automaticNameDelimiter: '~',
          priority: 9
        }
      }
    },
    // splitChunks: {
    //   cacheGroups: {
    //     elementUI: {
    //       name: "chunk-elementUI", // 单独将 elementUI 拆包
    //       priority: 15, // 权重需大于其它缓存组
    //       test: /[\/]node_modules[\/]element-ui[\/]/
    //     }
    //   }
    // },
    // 清除到代码中无用的js代码，只支持import方式引入，不支持commonjs的方式引入 只要mode是production就会生效
    usedExports: true,
    minimizer: [
      new ParallelUglifyPlugin({ // 多进程压缩
        cacheDir: '.cache/',
        sourceMap: false,
        test: /\.js(\?.*)?$/i,
        exclude: /node_modules/, // 屏蔽不需要处理的文件（文件夹）（可选）
        cache: true,
        cacheKeys: (defaultCacheKeys) => {
          defaultCacheKeys.myCacheKey = 'myCacheKeyValue'
          return defaultCacheKeys
        },
        chunkFilter: (chunk) => chunk.name === 'vendor',
        parallel: true,
        parallel: 4,
        sourceMap: false,
        uglifyJS: {
          output: {
            comments: false,
            beautify: false
          },
          compress: {
            // warnings: false,
            drop_console: true,
            collapse_vars: true,
            reduce_vars: true
          }
        }
      })
    ]
  }
})