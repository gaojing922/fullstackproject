const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ProgressBarPlugin = require('progress-bar-webpack-plugin') // 打包进度
const HappyPack = require('happypack')
const os = require('os') // node 提供的系统操作模块
const happyThreadPool = HappyPack.ThreadPool({size: os.cpus().length}) // 根据我的系统的内核数量 指定线程池个数 也可以其他数量
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const {title} = require('../src/platform.config')
const config = require('./config')
module.exports = {
  entry: {
    vendor: ['vue'], // vendor文件单独打包
    main: ['babel-polyfill', config.entryPath]
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.js$/,
        exclude: /node_modules/, // 排除不处理的目录
        include: path.join(__dirname, '../src'), // 精确指定要处理的目录
        use: 'happypack/loader?id=babel'
      },
      {
        test: /\.(png|jpg|gif|svg|jpeg|ico|woff|woff2|eot|ttf|otf)$/,
        // include: config.commonInclude,
        use: [
          {
            loader: 'url-loader', // 根据图片大小，把图片优化成base64 url-loader比file-loader更强大
            options: {
              limit: 1000, // 如果图片小于10000byte，就将图片转成base64
              name: '[name].[hash:8].[ext]',
              esModule: false, // 不设置false的话图片路径为模块对象
              outputPath: 'images/' //指定放置目标文件的文件系统路径
            }
          },
          {
            loader: 'image-webpack-loader', // 压缩图片
            options: {
              mozjpeg: {
                progressive: true,
                quality: 65
              },
              optipng: {
                enabled: true
              },
              pngquant: {
                quality: [0.65, 0.90],
                speed: 4
              },
              gifsicle: {
                interlaced: false
              }
              // webp: {
              //   quality: 75
              // }
            }
          }
        ]
      },
      {
        test:/\.html$/,
        loader:'html-withimg-loader'
      }
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new HappyPack({ // 基础参数设置
      id: 'babel', // 上面loader?后面指定的id
      loaders: ['babel-loader?cacheDirectory'], // 实际匹配处理的loader
      threadPool: happyThreadPool,
      verbose: true
    }),
    new ProgressBarPlugin(),
    new HtmlWebpackPlugin({ // 生成Html模版
      title,
      filename: 'index.html', // 默认值 index.html 最终生成在dist目录下面的html模版
      template: config.indexPath, // html模版源
      minify: {
        collapseWhitespace: true, // 是否去除空白
        removeComments: true, // 是否移除注释
        removeAttributeQuotes: true, // 移除属性的引号
        collapseBooleanAttributes: true // 省略只有 boolean 值的属性值 例如：readonly checked
      },
      favicon:''
    }),
  ],
  // 1.配置 webpack 寻找模块的规则
  resolve: {
    // 2.导入 require('Button')，会先在components下查找，然后再到node_modules下查找
    // 相对路径是相对于webpack.config.js文件所在的路劲
    modules:['../src','node_modules'],
    extensions: ['.js', '.json', '.vue'], // 模块的后缀名
    alias: {
      'src': path.join(__dirname, '../src'),
      'css': path.join(__dirname, '../src/assets/css'),
      'img': path.join(__dirname, '../src/assets/images'),
      'util': path.join(__dirname, '../src/assets/util'),
      'api': path.join(__dirname, '../src/api'),
      'lang': path.join(__dirname, '../src/lang'),
      'store': path.join(__dirname, '../src/store'),
      'pages': path.join(__dirname, '../src/pages')
    }
  }
}