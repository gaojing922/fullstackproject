const path = require('path')
const merge = require('webpack-merge')
const baseConfig = require('./webpack.base')
const webpack = require('webpack')
module.exports = merge(baseConfig, {
  mode: 'development', // 开发模式
  output: {
    filename: 'js/[name].[hash:8].js',
    path: path.join(__dirname, '../dist')
    // publicPath: '/' // 这里可以省略 path是相对于publicPath的 开发环境可以不用设置
  },
  devtool: 'inline-source-map', // 开发环境使用这个用来追踪js文件
  devServer: {
    clientLogLevel: 'warning',
    hot: true, // 启用webpack的模块热替换特性,这个需要配合webpack.HotModuleReplacementPlugin插件
    contentBase: path.join(__dirname, 'dist'), // 告诉服务器从哪里提供内容,默认情况下,将使用当前工作目录
    compress: true, // 一切服务都启用gzip压缩
    host: 'localhost', // 指定使用一个host，默认是localhost，如果希望服务器外部访问 0.0.0.0
    hotOnly:true,
    port: 1992, // 端口
    open: true, // 是否自动打开浏览器
    overlay: { // 出现错误或者警告的时候，是否覆盖页面线上错误消息
      warning: true,
      errors: true
    },
    publicPath: '/', // 此路径下的打包文件可在浏览器中访问
    proxy: { // 设置代理
      '/customApi': { // api是在axios(api/ajax.js文件里面)请求之后自定义加上去的，以方便所有接口统一代理，访问api开头的请求，会跳转到下面target配置 /aip/getuser ===> http://192.168.0.102:8080/mockjsdata/5/api/getuser
        target: 'http://127.0.0.1:2323/',
        secure: false,      // 如果是https接口，需要配置这个参数为true
        changeOrigin: true,
        // pathRewrite: {'^/api': '/mockjsdata/5/api'},
        pathRewrite: {
          '^/customApi': '/'
        }
      }
    },
    watchOptions: { // 监视文件相关的控制选项
      poll: true,
      ignored: /node_modules/, // 忽略监控的文件夹
      aggregateTimeout: 300 // 默认值，当第一个文件更改,会在重新构建提前增加延迟
    }
  },
  module: {
    rules: [
      {
        test: /\.(c|le)ss$/,
        use: [
          'style-loader',
          "vue-style-loader",
          {
            loader: 'css-loader',
            options: {
              sourceMap: true // 一般配置在开发环境 定位css在哪个文件
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              sourceMap: true,
              plugins: (loader) => [
                require('autoprefixer') // 给css3属性添加前缀
              ]
            }
          },
          'less-loader'
        ]
      }
    ]
  },
  plugins: [
    new webpack.NamedModulesPlugin(), // 更容易查看(patch)的依赖
    new webpack.HotModuleReplacementPlugin() // 替换插件
  ]
})