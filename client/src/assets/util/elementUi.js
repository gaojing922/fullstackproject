import Vue from 'vue'
import {
  Button,
  Icon,
  Pagination,
  Dialog,
  Notification,
  Image,
  Input,
  MessageBox
} from 'element-ui'
Vue.use(Button)
Vue.use(Icon)
Vue.use(Pagination)
Vue.use(Dialog)
Vue.use(Image)
Vue.use(Input)
Vue.prototype.$notify = Notification
Vue.prototype.$confirm = MessageBox.confirm
