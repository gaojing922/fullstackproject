import moment from 'moment'
// 格式化年月日
export const fromatNyr = (date) => {
  return moment(date).format('YYYY-MM-DD')
}
// 格式化年月日时分秒
export const fromatNyrSfm = (date) => {
  return moment(date).format('YYYY-MM-DD HH:mm:ss')
}
