import Vue from 'vue'
import loading from 'pages/base/loading'
const Loading = {
  install: function (Vue) {
    Vue.component('Loading', loading)
  }
}
Vue.use(Loading)