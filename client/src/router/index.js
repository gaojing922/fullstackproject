import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
const Home = () => import('pages/components/home')
const Login = () => import('pages/components/login')
const Regist = () => import('pages/components/regist')
const Head = () => import('pages/base/wHeader')
const Foot = () => import('pages/base/wFooter')
const Error = () => import('pages/base/Error')
const getRouter = (comp) => ({
  default: comp,
  Head,
  Foot
})
const router = new VueRouter({
  routes: [
    {
      path: '/',
      name: 'Home',
      components: getRouter(Home)
    },
    {
      path: '/login',
      name: 'Login',
      components: getRouter(Login)
    },
    {
      path: '/regist',
      name: 'Regist',
      components: getRouter(Regist)
    },
    {
      path: '*',
      name: 'error',
      components: getRouter(Error)
    }
  ]
})
export default router