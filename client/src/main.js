import "babel-polyfill"
import Vue from 'vue'
import router from './router'
import store from 'store'
import App from './App.vue'
import {mixins} from './mixins'
import {i18n} from 'src/lang' // 导入国际化
import 'css/global.less'
import '../src/assets/font/iconfont.css'
import 'util/elementUi'
// Loading全局组件
import 'util/loading'
Vue.mixin(mixins)
Vue.config.productionTip = false
new Vue({
  el: "#app",
  router,
  store,
  i18n,
  render: h => h(App)
})