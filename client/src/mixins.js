export const mixins = {
  data() {
    return {
    }
  },
  methods: {
    // 访问外部资源路径
    imgPath(img) {
      try {
        return require('img/' + img)
      } catch (e) {
        console.error(e)
        return ''
      }
    }
  }
}
