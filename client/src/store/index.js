import Vue from 'vue'
import Vuex from 'vuex'
import initState from './initStore'
Vue.use(Vuex)
const state = () => {
  return {
    islogin: false,
    userInfo: {}
  }
}
const getters = {
  islogin: state => state.islogin,
  userInfo: state => state.userInfo
}
const mutations = {
  SET_ISLOGIN (state, data) {
    state.islogin = data
  },
  SET_USER_INFO (state, data) {
    state.userInfo = data
  }
}
const actions = {
  AsetLoginStatus({commit, state}, bol) {
    commit('SET_ISLOGIN', bol)
  },
  AsetUserInfo({commit, state}, obj) {
    commit('SET_USER_INFO', obj)
  },
}
export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  modules: {
    initState
  }
})