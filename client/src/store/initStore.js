// 项目初始化需要加载的状态
const state = () => ({
  loginDialog: false,
  registDialog: false
})
const getters = {
  loginDialog: state => state.loginDialog,
  registDialog: state => state.registDialog
}
const mutations = {
  SET_LOGING_DIALOG (state, bol) {
    state.loginDialog = bol
  },
  SET_REGIST_DIALOG (state, bol) {
    state.registDialog = bol
  }
}
const actions = {
  AsetLoginDialog({commit, state}, bol) {
    commit('SET_LOGING_DIALOG', bol)
  },
  AsetRegistDialog({commit, state}, bol) {
    commit('SET_REGIST_DIALOG', bol)
  }
}
export default {
  state,
  getters,
  mutations,
  actions
}