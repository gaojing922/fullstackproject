// 中文
module.exports = {
  Header: {
    time: '当前系统时间',
    zh: '中文',
    en: 'English',
    Sunday: '星期日',
    Monday: '星期一',
    Tuesday: '星期二',
    Wednesday: '星期三',
    Thursday: '星期四',
    Friday: '星期五',
    Saturday: '星期六',
    login: '登录',
    regist: '注册',
    home: '首页'
  },
  Home: {
    signature: '爱就好比骑马和学英语, 如果不趁年轻时学会, 以后想学就难了'
  }
}