// 国际化  页面获取通过$t('Header.title')此种写法
import Vue from 'vue'
import VueI18n from 'vue-i18n'
import zh from './zh'
import en from './en'
import storage from 'good-storage'
Vue.use(VueI18n)
export const i18n = new VueI18n({
  locale: storage.get('lang') || 'zh', // 语言标识 默认中文
  messages: {
    zh,
    en
  }
})