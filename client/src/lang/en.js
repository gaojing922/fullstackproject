// 英文
module.exports = {
  Header: {
    time: 'Current system time',
    zh: '中文',
    en: 'English',
    Sunday: 'Sunday',
    Monday: 'Monday',
    Tuesday: 'Tuesday',
    Wednesday: 'Wednesday',
    Thursday: 'Thursday',
    Friday: 'Friday',
    Saturday: 'Saturday',
    login: 'Login',
    regist: 'Regist',
    home: 'Home'
  },
  Home: {
    signature: "Love is like riding or speaking French,if you don not learn it young, it's hard to get the trick of it later."
  }
}