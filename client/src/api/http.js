import axios from 'axios'
import router from 'router'
import store from 'store'
import { Notification } from 'element-ui'
axios.defaults.timeout = 10000
axios.defaults.baseURL = process.env.NODE_ENV === 'production' ? '' : '/customApi'
// post请求的时候，我们需要加上一个请求头，所以可以在这里进行一个默认的设置，即设置post的请求头为application/x-www-form-urlencoded;charset=UTF-8
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';

// instance.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
// 常见的http状态码信息,根据需求设置
// const httpCode = {
//   400: '请求参数错误',
//   401: '权限不足, 请重新登录',
//   403: '服务器拒绝本次访问',
//   404: '请求资源未找到',
//   500: '内部服务器错误',
//   501: '服务器不支持该请求中使用的方法',
//   502: '网关错误',
//   504: '网关超时'
// }
// // 每个请求都要携带的固定的参数
// // const platformKey = '这里写固定的参数'
// // 定义后台返回的状态标识, 可以跟后台商量
// const SUCCESS_CODE = 'ok'
// // 添加请求拦截器
// instance.interceptors.request.use(config => {
//   config.headers['token'] = sessionStorage.getItem('token') || '' // 在header中携带参数
//   // loadingInstance = Loading.service({       // 发起请求时加载全局loading，请求失败或有响应时会关闭
//   //   spinner: 'fa fa-spinner fa-spin fa-3x fa-fw',
//   //   text: '拼命加载中...'
//   // })
//   // 在这里：可以根据业务需求可以在发送请求之前做些什么:例如我这个是导出文件的接口，因为返回的是二进制流，所以需要设置请求响应类型为blob，就可以在此处设置。
//   if (config.url.includes('pur/contract/export')) {
//     config.headers['responseType'] = 'blob'
//   }
//   // 我这里是文件上传，发送的是二进制流，所以需要设置请求头的'Content-Type'
//   if (config.url.includes('pur/contract/upload')) {
//     config.headers['Content-Type'] = 'multipart/form-data'
//   }
//   return config
// }, error => {
//   // 对请求错误做些什么
//   return Promise.reject(error)
// })

// // 添加响应拦截器
// instance.interceptors.response.use(response => {
//   // loadingInstance.close()
//   // 响应结果里的status
//   if (response.data.code === SUCCESS_CODE) {
//     return Promise.resolve(response.data)
//   } else {
//     // Message({
//     //   message: response.data.message,
//     //   type: 'error'
//     // })
//     return Promise.reject(response.data.message)
//   }
// }, error => {
//   // loadingInstance.close()
//   if (error.response) {
//     // 根据请求失败的http状态码去给用户相应的提示
//     let tips = error.response.status in httpCode ? httpCode[error.response.status] : error.response.data.message
//     // Message({
//     //   message: tips,
//     //   type: 'error'
//     // })
//     // token或者登陆失效情况下跳转到登录页面，根据实际情况，在这里可以根据不同的响应错误结果，做对应的事。这里我以401判断为例
//     if (error.response.status === 401) {
//       router.push({
//         path: '/login'
//       })
//     }
//     return Promise.reject(error)
//   } else {
//     // Message({
//     //   message: '请求超时, 请刷新重试',
//     //   type: 'error'
//     // })
//     return Promise.reject(new Error('请求超时, 请刷新重试'))
//   }
// })

// 未登录跳转至登录页面
const toLogin = () => {
  router.replace({
    path: '/',
    query: {
      redirect: router.currentRoute.fullPath
    }
  })
}
// 提示
const tipMessage = (message, title = '提示') => {
  Notification.error({
    title,
    message,
    duration: 0
  })
}
// 错误处理
const errorHandle = (status, other) => {
  // 状态码判断
  switch (status) {
    // 401: 未登录状态，跳转登录页
    case 401:
      // toLogin()
      tipMessage('未登录')
      break;
    // 403 token过期
    // 清除token并跳转登录页
    case 403:
      tipMessage('登录过期，请重新登录')
      localStorage.removeItem('token')
      store.commit('loginSuccess', null)
      setTimeout(() => {
        toLogin()
      }, 1000)
      break
    // 404请求不存在
    case 404:
      tipMessage('请求的资源不存在')
      break
    default:
      console.log(other)
  }
}
/**
 * 请求拦截器
 * 每次请求前，如果存在token则在请求头中携带token
 */
axios.interceptors.request.use(config => {
  // 登录流程控制中，根据本地是否存在token判断用户的登录情况
  // 但是即使token存在，也有可能token是过期的，所以在每次的请求头中携带token
  // 后台根据携带的token判断用户的登录情况，并返回给我们对应的状态码
  // 而后我们可以在响应拦截器中，根据状态码进行一些统一的操作
  const token = store.state.token
  token && (config.headers.Authorization = token)
  return config
}, error => Promise.error(error)
)

// 响应拦截器
axios.interceptors.response.use(
  // 请求成功
  res => res.status === 200 ? Promise.resolve(res) : Promise.reject(res),
  // 请求失败
  error => {
    const { response } = error
    if (response) {
      // 请求已发出，但是不在2xx的范围
      errorHandle(response.status, response.data.message)
      return Promise.reject(response)
    } else {
      // 处理断网的情况
      // eg:请求超时或断网时，更新state的network状态
      // network状态在app.vue中控制着一个全局的断网提示组件的显示隐藏
      // 关于断网组件中的刷新重新获取数据，会在断网组件中说明
      if (!window.navigator.onLine) {
        store.commit('changeNetwork', false)
      } else {
        return Promise.reject(error)
      }
    }
})

// 统一封装get post 请求, 如果有其它请求，可以加
/*
* url 请求地址
* method 请求方式
* formdata 请求参数 {}
*/
const axiosHttp = ({ url, type, formdata = {} }) => {
  type = type && type.toUpperCase() || 'GET'
  // 如果需要在每个请求的参数都携带一个key，就打开下面的
  // formdata = Object.assign({}, formdata, {
  //   platformKey
  // })
  // 判断请求方式
  if (type === 'GET') {
    // 拼接字符串
    let parmasStr = ''
    Object.keys(formdata).forEach(key => {
      parmasStr += key + '=' + formdata[key] + '&'
    })
    // 过滤掉最后的&
    if (parmasStr !== '') {
      parmasStr = parmasStr.substr(0, parmasStr.lastIndexOf('&'))
      // 拼接完整的路径
      url += '?' + parmasStr
    }
    // formdata.timeStamp = new Date().getTime()
    return axios.get(url).then((res) => {
      return Promise.resolve(res ? res.data : {})
    })
  } else if (type === 'POST') {
    return axios.post(url, formdata).then((res) => {
      return Promise.resolve(res ? res.data : {})
    })
  }
}
export default axiosHttp