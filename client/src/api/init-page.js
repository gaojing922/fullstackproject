// 请求接口按这个格式
import axiosHttp from './http'
// 获取评论列表
export const getList = (data) => {
  return axiosHttp({
    url: '/getList',
    type: 'get',
    formdata: data
  })
}
// 登录
export const login = (data) => {
  return axiosHttp({
    url: '/login',
    type: 'post',
    formdata: data
  })
}

// 退出
export const logout = (data) => {
  return axiosHttp({
    url: '/logout',
    type: 'post',
    formdata: data
  })
}

// 决断用户是否登录
export const isLogin = () => {
  return axiosHttp({
    url: '/isLogin',
    type: 'get'
  })
}

// 注册
export const register = (data) => {
  return axiosHttp({
    url: '/register',
    type: 'post',
    formdata: data
  })
}

// 点赞
export const like = (data) => {
  return axiosHttp({
    url: '/like',
    type: 'post',
    formdata: data
  })
}
