/**
 * 页面数据配置
 * */
import { i18n } from 'lang'
// 星期
// export const week = ['星期日', '星期一', '星期二', '星期三',  i18n.tc('Header.Thursday'), '星期五', '星期六']
export const week = [
  i18n.tc('Header.Sunday'),
  i18n.tc('Header.Monday'),
  i18n.tc('Header.Tuesday'),
  i18n.tc('Header.Wednesday'),
  i18n.tc('Header.Thursday'),
  i18n.tc('Header.Friday'),
  i18n.tc('Header.Saturday')
]